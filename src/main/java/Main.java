import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException {
        Car car = new Car();
        System.out.println("Object " + car.getClass() + " before using reflection: ");
        car.toString();

        Class clazz = car.getClass();

        Field [] fields = clazz.getDeclaredFields();
        for (Field f : fields){
            if (f.isAnnotationPresent(MyFieldAnnotation.class)){
                MyFieldAnnotation annotation = f.getAnnotation(MyFieldAnnotation.class);
                f.setAccessible(true);
                try {
                    f.set(car, annotation.name());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }

        }

        Method [] methods = clazz.getDeclaredMethods();
        for (Method m : methods){
            if (m.getName().equals("drive")){
                Annotation annotation = m.getAnnotation(MyMethodAnnotation.class);
                m.setAccessible(true);
                try {
                    m.invoke(car, ((MyMethodAnnotation) annotation).speedUP());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Object " + car.getClass() + " after using reflection: ");
        car.toString();
    }
}
