public class Car {
    @MyFieldAnnotation(name = "test")
    private String name;
    private int speed;

    public Car() {
    }

    @MyMethodAnnotation(speedUP = 5)
    public boolean drive(int speedUp) {
        this.speed = this.speed + speedUp;
        return true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        System.out.println("Car {" +
                " name = '" + this.getName() + '\'' +
                ", speed = " + this.getSpeed() +
                '}'+ "\n");
        return null;
    }
}
